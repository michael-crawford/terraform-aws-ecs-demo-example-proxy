# Terraform AWS ECS Demo Example Proxy

ECS Demo Example Proxy Container Image

This container uses NGINX to create a proxy for the example recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

## Build

### Create container image for the Recipe Application Proxy Component

1. Create a feature branch to add the nginx-proxy

    This is the method to create the branch in the client via the command line.
    Note there is also a way to do this via VS Code, by clicking on the status bar branch name, then clicking the
    "+ Create a new branch" option in the menu which comes up.

    ```bash
    cd ~/src/michael-crawford/terraform-aws-ecs-demo-example-proxy
    git checkout -b feature/nginx-proxy
    ```

1. Add the following files from the original code

   Add these files using VS Code

    * Dockerfile
    * default.conf.tpl
    * uwsgi_params
    * entrypoint.sh

1. Run Docker Desktop

    * Install the latest version of Docker Desktop for macOS if not already installed.
    * Run Docker desktop

1. Build the initial Docker image

    Confirm this builds correctly on the local workstation

    ```bash
    docker build -t proxy .
    ```

1. Commit these initial files to build the Docker image

    ```bash
    git add Dockerfile default.conf.tpl uwsgi_params entrypoint.sh
    git commit -m "Added NGINX proxy"
    ```

1. Add the following files from the original code

   Add these files using VS Code

    * .gitlab-ci.yml

1. Commit these initial files to build the Docker image

    ```bash
    git add .gitlab-ci.yml
    git commit -m "Added CI pipeline"
    ```

1. Push the new branch to GitLab

    Since this branch does not yet exist on the GitLab, on the initial push we need to indicate that a new branch of the
    same name should be created on the origin. Display all local and remote branches when we're done to confirm the new
    branch exits in both locations.

    ```bash
    git push --set-upstream origin feature/nginx-proxy

    git branch -a
    ```



## References

* [Introduction to GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
* [Predefined variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
* [.gitlab-ci.yml keyword reference #Rules](https://docs.gitlab.com/ee/ci/yaml/#rules)